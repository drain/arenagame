﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Stats))]
public class Entity : MonoBehaviour
{
    public int id;
    public string eName;
    public Entity target;
    public Spells spells;
    public Stats stats;

    public Canvas nameplate;
    public Slider healthBar;
    public Text nameText;
    private Camera m_playerCam;
    public Animator anim;

    protected virtual void Awake()
    {
        spells = GetComponent<Spells>();
        stats = GetComponent<Stats>();

        m_playerCam = FindObjectOfType<Camera>();
    }

    protected virtual void OnMouseDown()
    {
        //send server a message that this client clicked this character
        ClientSend.UpdateTarget(this.id);
    }

    private void Update()
    {
        if (nameplate != null)
            UpdateNameplate();
    }

    public void UpdateNameplate()
    {
        if(nameplate.gameObject.activeSelf)
            nameplate.transform.LookAt(nameplate.transform.position + m_playerCam.transform.rotation * Vector3.forward,
                                       m_playerCam.transform.rotation * Vector3.up);

        float dist = Vector3.Distance(nameplate.transform.position, m_playerCam.transform.position);

        //TODO: make healtbar drawdistance and other options

        if (dist > 35)
            nameplate.gameObject.SetActive(false);
        else
        {
            if(!nameplate.gameObject.activeSelf)
                nameplate.gameObject.SetActive(true);

            nameplate.transform.localScale = new Vector3(0.015f, 0.015f, 1) * dist / 13.5f;
        }

        /*float dist = Vector3.Distance(nameText.transform.position, m_playerCam.transform.position);
        nameText.transform.localScale = Vector3.one * dist / 8;*/
    }

    public void UpdateHealthBar()
    {
        healthBar.value = stats.health / stats.maxHealth;
    }
}
