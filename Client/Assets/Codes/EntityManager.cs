﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour
{
    public static EntityManager instance;
    public static Dictionary<int, Entity> entities;

    public List<Entity> entityPrefabs = new List<Entity>();

    private void Awake()
    {
        instance = this;
        entities = new Dictionary<int, Entity>();
    }

    //return entity based on its id (this is not playerId!!)
    public Entity GetEntity(int id)
    {
        for (int i = 0; i < entities.Count; i++)
        {
            if (entities[i].id == id)
                return entities[i];
        }

        return null;
    }

    public Entity GetEntityPrefab(int entityId)
    {
        for (int i = 0; i < entityPrefabs.Count; i++)
        {
            if (entityPrefabs[i].id == entityId)
                return entityPrefabs[i];
        }

        return null;
    }

    //used for anything that is not player (npcs, player pets, etc.)
    public Entity SpawnEntity(int prefabId, int entityId, string entityName, Vector3 pos, Quaternion rot)
    {
        Entity prefab = GetEntityPrefab(prefabId);
        if(prefab != null)
        {
            Entity e = Instantiate(prefab.gameObject, pos, rot).GetComponent<Entity>();
            e.eName = entityName;
            e.id = entityId;
            e.nameText.text = e.eName;
            if(!entities.ContainsKey(e.id))
                entities.Add(e.id, e);
            return e;
        }

        return null;
    }

    public void RemoveEntity(int id)
    {
        entities.Remove(id);
    }
}
