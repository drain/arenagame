﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    private static void SendTCPData(Packet packet)
    {
        packet.WriteLength();
        Client.instance.tcp.SendData(packet);
    }

    private static void SendUDPData(Packet packet)
    {
        packet.WriteLength();
        Client.instance.udp.SendData(packet);
    }

    #region Packets
    public static void WelcomeReceived()
    {
        using (Packet packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            packet.Write(Client.instance.myId);
            packet.Write(FindObjectOfType<UIManager>().userNameField.text);

            SendTCPData(packet);
        }
    }

    public static void CharacterLoaded()
    {
        using (Packet packet = new Packet((int)ClientPackets.characterLoaded))
        {
            SendTCPData(packet);
        }
    }

    public static void PlayerMovement(bool[] inputs)
    {
        using (Packet packet = new Packet((int)ClientPackets.playerMovement))
        {
            packet.Write(inputs.Length);
            for(int i = 0; i < inputs.Length; i++)
            {
                packet.Write(inputs[i]);
            }

            packet.Write(GameManager.players[Client.instance.myId].transform.rotation);

            SendUDPData(packet);
        }
    }

    public static void UpdateTarget(int targetId)
    {
        using (Packet packet = new Packet((int)ClientPackets.updateTarget))
        {
            packet.Write(targetId);

            SendTCPData(packet);
        }
    }

    public static void ToggleAttack(bool toggleAttack)
    {
        using (Packet packet = new Packet((int)ClientPackets.toggleAttack))
        {
            packet.Write(toggleAttack);

            SendTCPData(packet);
        }
    }

    public static void UseSpell(int spellId, int targetId)
    {
        using (Packet packet = new Packet((int)ClientPackets.castSpell))
        {
            packet.Write(targetId);
            packet.Write(spellId);

            SendTCPData(packet);
        }
    }

    #endregion
}
