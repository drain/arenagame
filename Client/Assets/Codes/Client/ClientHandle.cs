﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class ClientHandle : MonoBehaviour
{
    public static void Welcome(Packet packet)
    {
        //must read in same order as sent
        string msg = packet.ReadString();
        int id = packet.ReadInt();

        Debug.Log("Server:" + msg);
        Client.instance.myId = id;

        ClientSend.WelcomeReceived();

        //Debug.Log(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).ToString());
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
        UIManager.WriteToLog("Connected to server!");
        UIManager.WriteToLog("(SERVER): " + msg);
    }

    public static void SpawnPlayer(Packet packet)
    {
        int id = packet.ReadInt();
        int entityId = packet.ReadInt();
        string username = packet.ReadString();
        Vector3 pos = packet.ReadVector3();
        Quaternion rot = packet.ReadQuaternion();

        GameManager.GM.SpawnPlayer(id, entityId, username, pos, rot);
        ClientSend.CharacterLoaded();
        UIManager.WriteToLog(username + " joined the game.");
    }

    public static void PlayerPosition(Packet packet)
    {
        int id = packet.ReadInt();
        Vector3 pos = packet.ReadVector3();

        if (GameManager.players.ContainsKey(id))
        {
            if (id != GameManager.GM.localPlayer.id)
            {
                if (pos != GameManager.players[id].transform.position)
                    GameManager.players[id].anim.SetFloat("Speed", 1);
                else
                    GameManager.players[id].anim.SetFloat("Speed", 0);
            }

            GameManager.players[id].transform.position = Vector3.Lerp(GameManager.players[id].transform.position, pos, 0.5f);
        }
    }

    public static void PlayerRotation(Packet packet)
    {
        int id = packet.ReadInt();
        Quaternion rot = packet.ReadQuaternion();

        if (GameManager.players.ContainsKey(id))
            GameManager.players[id].transform.rotation = rot;
    }

    public static void PlayerDisconnected(Packet packet)
    {
        int id = packet.ReadInt();

        if (GameManager.players.ContainsKey(id))
        {
            UIManager.WriteToLog(GameManager.players[id].username + " disconnected.");
            if(GameManager.players[id] != null && GameManager.players[id].gameObject != null)
                Destroy(GameManager.players[id].gameObject);
            GameManager.players.Remove(id);
        }   
    }

    public static void SpawnEntity(Packet packet)
    {
        int prefabId = packet.ReadInt();
        int id = packet.ReadInt();
        string name = packet.ReadString();
        Vector3 pos = packet.ReadVector3();
        Quaternion rot = packet.ReadQuaternion();

        EntityManager.instance.SpawnEntity(prefabId, id, name, pos, rot);
    }

    public static void EntityPosition(Packet packet)
    {
        int id = packet.ReadInt();
        Vector3 pos = packet.ReadVector3();

        if (EntityManager.entities.ContainsKey(id))
            EntityManager.entities[id].transform.position = pos;
    }

    public static void EntityRotation(Packet packet)
    {
        int id = packet.ReadInt();
        Quaternion rot = packet.ReadQuaternion();

        if (EntityManager.entities.ContainsKey(id))
            EntityManager.entities[id].transform.rotation = rot;
    }

    public static void EntityRemove(Packet packet)
    {
        int id = packet.ReadInt();

        if (EntityManager.entities.ContainsKey(id))
        {
            Entity e = EntityManager.entities[id];
            UIManager.WriteToLog(e.eName + " died.");
            EntityManager.entities.Remove(id);
            Destroy(e.gameObject);
        }
    }

    public static void UpdateTarget(Packet packet)
    {
        int entityId = packet.ReadInt();
        int targetId = packet.ReadInt();

        if (EntityManager.entities.ContainsKey(entityId))
        {
            if (targetId == -1)
            {
                EntityManager.entities[entityId].target = null;
            }
            else if(EntityManager.entities.ContainsKey(targetId))
            {
                EntityManager.entities[entityId].target = EntityManager.entities[targetId];
            }

            //todo check if it involves us before updating?
            UIManager.instance.UpdateTargetUI();
        }
    }

    public static void UpdateStats(Packet packet)
    {
        int entityId = packet.ReadInt();
        float health = packet.ReadFloat();
        float maxHealth = packet.ReadFloat();
        float mana = packet.ReadFloat();
        float maxMana = packet.ReadFloat();

        if(EntityManager.entities.ContainsKey(entityId))
        {
            Entity entity = EntityManager.entities[entityId];
            entity.stats.health = health;
            entity.stats.maxHealth = maxHealth;

            entity.stats.mana = mana;
            entity.stats.maxMana = maxMana;

            //same as above
            UIManager.instance.UpdateHealthBars();
            entity.UpdateHealthBar();

            if(entity.stats.health <= 0)
            {
                entity.anim.SetTrigger("Death");
            }
        }
    }

    public static void ToggleAttack(Packet packet)
    {
        int playerId = packet.ReadInt();
        bool toggleAttack = packet.ReadBool();

        //TODO: make this less complicated
        if(EntityManager.entities.ContainsKey(playerId))
        {
            if(EntityManager.entities[playerId].GetType() == typeof(PlayerManager))
            {
                PlayerManager pm = (PlayerManager)EntityManager.entities[playerId];
                if(pm.playerId == GameManager.GM.localPlayer.playerId)
                {
                    UIManager.WriteToLog("Auto attack toggled " + ((toggleAttack) ? "on." : "off."));
                }
            } 
        }
    }

    public static void UseSpell(Packet packet)
    {
        int entityId = packet.ReadInt();
        int targetId = packet.ReadInt();
        int spellId = packet.ReadInt();

        if(EntityManager.entities.ContainsKey(entityId))
        {
            if (targetId == -1)
                EntityManager.entities[entityId].spells.UseSpell(spellId);
            else if (EntityManager.entities.ContainsKey(targetId))
                EntityManager.entities[entityId].spells.UseSpell(spellId, EntityManager.entities[targetId]);
        }
    }

    public static void SpellCancelled(Packet packet)
    {
        int entityId = packet.ReadInt();
        bool interrupted = packet.ReadBool();

        if (EntityManager.entities.ContainsKey(entityId))
            EntityManager.entities[entityId].spells.StopCasting(interrupted);
    }

    public static void SpellFinished(Packet packet)
    {
        int entityId = packet.ReadInt();
        int targetId = packet.ReadInt();
        int spellId = packet.ReadInt();
        float value = packet.ReadFloat();

        if (EntityManager.entities.ContainsKey(entityId))
        {
            Entity user = EntityManager.entities[entityId];

            user.anim.SetTrigger("Shoot2");

            if (targetId == -1)
            {
                user.spells.CastFinished(spellId);
            }  
            else if (EntityManager.entities.ContainsKey(targetId))
            {
                Entity target = EntityManager.entities[targetId];
                user.spells.CastFinished(spellId, target);
            }

            //TODO: implement friendly and enemy targets
            if(!user.spells.GetSpell(spellId).friendlySpell && targetId != -1)
            {
                UIManager.WriteToLog(user.eName + " casted "
                                   + user.spells.GetSpell(spellId).spellName + " on "
                                   + EntityManager.entities[targetId].eName
                                   + ((value != 0) ? " for " + value : "."));

                if(value != 0 && user.id == GameManager.GM.localPlayer.id)
                    UIManager.instance.SpawnSpellDamageNumber(EntityManager.entities[targetId], value);
                if(value != 0 && EntityManager.entities[targetId].id == GameManager.GM.localPlayer.id)
                    UIManager.instance.SpawnDamageTakenNumber(EntityManager.entities[targetId], value);
            }
            else
            {
                UIManager.WriteToLog(user.eName + " casted "
                                  + user.spells.GetSpell(spellId).spellName
                                  + ((value != 0) ? " for " + value : "."));

                if (value != 0 && user.id == GameManager.GM.localPlayer.id)
                    UIManager.instance.SpawnHealNumber(user, value);
            }
        }
    }

    public static void AutoAttack(Packet packet)
    {
        int entityId = packet.ReadInt();
        int userId = packet.ReadInt();
        float value = packet.ReadFloat();

        if(EntityManager.entities.ContainsKey(entityId) && EntityManager.entities.ContainsKey(userId))
        {
            Entity entity = EntityManager.entities[entityId];
            Entity user = EntityManager.entities[userId];

            UIManager.WriteToLog(user.eName + " melee swing did " + value + " damage to " + entity.eName);
            if(user.id == GameManager.GM.localPlayer.id)
                UIManager.instance.SpawnAutoAttackNumber(entity, value);
            else if (entityId == GameManager.GM.localPlayer.id)
                UIManager.instance.SpawnDamageTakenNumber(entity, value);

            //TODO: have animations be based on ids like 0 move, 1 run, 2 attack, 3 attack2 etc
            // or something
            user.anim.SetTrigger("Shoot");
        }
    }
}
