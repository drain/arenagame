﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;

public class Client : MonoBehaviour
{
    public static Client instance;
    public static int dataBufferSize = 4096;

    public string ip = "127.0.0.1";
    public int port = 8150;
    public int myId = 0;
    public TCP tcp;
    public UDP udp;

    public const int SIO_UDP_CONNRESET = -1744830452;

    private bool isConnected = false;
    private delegate void PacketHandler(Packet packet);
    private static Dictionary<int, PacketHandler> packetHandlers;

    private void Awake()
    {
        //singleton
        if(instance == null)
        {
            instance = this;
        }
    }

    public void Start()
    {
        tcp = new TCP();
        udp = new UDP();
    }

    private void OnApplicationQuit()
    {
        Disconnect();
    }

    public void ConnectToServer()
    {
        ip = UIManager.instance.serverIP.text;
        InitializeClientData();
        isConnected = true;
        //set the new IP to udp aswell <.<
        udp = new UDP();
        tcp.Connect();
        UIManager.instance.menuCamera.SetActive(false);
    }

    public class TCP
    {
        public TcpClient socket;

        private NetworkStream stream;
        private Packet receiveData;
        private byte[] receiveBuffer;

        public void Connect()
        {
            socket = new TcpClient();
            socket.ReceiveBufferSize = dataBufferSize;
            socket.SendBufferSize = dataBufferSize;

            receiveBuffer = new byte[dataBufferSize];
            socket.BeginConnect(instance.ip, instance.port, ConnectCallback, socket);
        }

        private void ConnectCallback(IAsyncResult result)
        {
            socket.EndConnect(result);

            if (!socket.Connected)
                return;

            stream = socket.GetStream();

            receiveData = new Packet();

            stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
        }

        public void SendData(Packet packet)
        {
            try
            {
                if(socket != null)
                {
                    stream.BeginWrite(packet.ToArray(), 0, packet.Length(), null, null);
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        private void ReceiveCallback(IAsyncResult result)
        {
            try
            {
                int byteLength = stream.EndRead(result);
                if (byteLength <= 0)
                {
                    instance.Disconnect();
                    return;
                }

                byte[] data = new byte[byteLength];
                Array.Copy(receiveBuffer, data, byteLength);

                receiveData.Reset(HandleData(data));

                //keep reading
                stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);

            }
            catch (Exception e)
            {
                //Debug.Log("Error receiving TCP data: " + e);
                Disconnect();
            }
        }

        private bool HandleData(byte[] data)
        {
            int packetLenght = 0;

            receiveData.SetBytes(data);

            //first one must be int for the length
            if(receiveData.UnreadLength() >= 4)
            {
                packetLenght = receiveData.ReadInt();
                if (packetLenght <= 0)
                    return true;
            }

            //handle packets
            while(packetLenght > 0 && packetLenght <= receiveData.UnreadLength())
            {
                byte[] packetBytes = receiveData.ReadBytes(packetLenght);
                ThreadManager.ExecuteOnMainThread(() =>
                {
                    using (Packet packet = new Packet(packetBytes))
                    {
                        int packetId = packet.ReadInt();
                        packetHandlers[packetId](packet);
                    }
                });

                packetLenght = 0;
                if (receiveData.UnreadLength() >= 4)
                {
                    packetLenght = receiveData.ReadInt();
                    if (packetLenght <= 0)
                        return true;
                }
            }

            if(packetLenght <= 1)
            {
                return true;
            }

            return false;
        }

        private void Disconnect()
        {
            instance.Disconnect();

            stream = null;
            receiveData = null;
            receiveBuffer = null;
            socket = null;
        }
    }

    public class UDP
    {
        public UdpClient socket;
        public IPEndPoint endPoint;

        public UDP()
        {
            //FUCKING KILL ME
            endPoint = new IPEndPoint(IPAddress.Parse(instance.ip), instance.port);
        }

        public void Connect(int localPort)
        {
            socket = new UdpClient(localPort);

            socket.Client.IOControl(
            (IOControlCode)SIO_UDP_CONNRESET,
            new byte[] { 0, 0, 0, 0 },
            null
            );

            socket.Connect(endPoint);
            socket.BeginReceive(ReceiveCallback, null);

            /*using (Packet packet = new Packet())
            {
                SendData(packet);
            }*/
        }

        public void SendData(Packet packet)
        {
            try
            {
                packet.InsertInt(instance.myId);
                if(socket != null)
                {
                    socket.BeginSend(packet.ToArray(), packet.Length(), null, null);
                }
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        private void ReceiveCallback(IAsyncResult result)
        {
            try
            {
                byte[] data = socket.EndReceive(result, ref endPoint);
                socket.BeginReceive(ReceiveCallback, null);

                if(data.Length < 4)
                {
                    instance.Disconnect();
                    return;
                }

                HandleData(data);

            }
            catch
            {
                Disconnect();
            }
        }

        private void HandleData(byte[] data)
        {
            using (Packet packet = new Packet(data))
            {
                int packetLength = packet.ReadInt();
                data = packet.ReadBytes(packetLength);
            }

            ThreadManager.ExecuteOnMainThread(() =>
            {
                using (Packet packet = new Packet(data))
                {
                    int packetId = packet.ReadInt();
                    packetHandlers[packetId](packet);
                }
            });
        }

        private void Disconnect()
        {
            instance.Disconnect();

            endPoint = null;
            socket = null;
        }
    }

    private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            {(int)ServerPackets.welcome, ClientHandle.Welcome },
            {(int)ServerPackets.spawnPlayer, ClientHandle.SpawnPlayer },
            {(int)ServerPackets.playerPosition, ClientHandle.PlayerPosition },
            {(int)ServerPackets.playerRotation, ClientHandle.PlayerRotation },
            {(int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },

            {(int)ServerPackets.spawnEntity, ClientHandle.SpawnEntity },
            {(int)ServerPackets.entityPosition, ClientHandle.EntityPosition },
            {(int)ServerPackets.entityRotation, ClientHandle.EntityRotation },
            {(int)ServerPackets.entityRemove, ClientHandle.EntityRemove },
            {(int)ServerPackets.updateTarget, ClientHandle.UpdateTarget },

            {(int)ServerPackets.updateStats, ClientHandle.UpdateStats },
            {(int)ServerPackets.toggleAttack, ClientHandle.ToggleAttack },
            {(int)ServerPackets.AutoAttack, ClientHandle.AutoAttack },
            {(int)ServerPackets.castSpell, ClientHandle.UseSpell },
            {(int)ServerPackets.castCancelled, ClientHandle.SpellCancelled },
            {(int)ServerPackets.castFinished, ClientHandle.SpellFinished },
        };
        Debug.Log("Packets initialized!");
    }

    private void Disconnect()
    {
        if(isConnected)
        {
            isConnected = false;
            tcp.socket.Close();
            udp.socket.Close();

            Debug.Log("Disconnected from server.");
        }

    }
}
