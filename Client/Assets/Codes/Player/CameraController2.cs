﻿using UnityEngine;
using System.Collections;

public class CameraController2 : MonoBehaviour
{
    private Transform m_transform;
    public Transform playerTransform;

    private float m_x;
    private float m_y;

    public float xSpeed = 5;
    public float ySpeed = 5;

    public float minY = -10, maxY = 85;

    private Vector3 m_angles;

    public float distanceMin, distanceMax = 15;

    public float distance = 10;
    public float scrollSpeed = 5;

    private Vector3 velocity;

    public float followSpeed;

    private void Awake()
    {
        m_transform = transform;

        m_angles = m_transform.eulerAngles;
        m_x = m_angles.y;
        m_y = m_angles.x;
    }

    public enum Mode
    {
        rotatePlayer,
        ignorePlayer
    };

    private void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1))
        {
            m_x += Input.GetAxis("Mouse X") * xSpeed;
            m_y -= Input.GetAxis("Mouse Y") * ySpeed;
        }
       
        m_y = ClampAngle(m_y, minY, maxY);

        Quaternion rotation = Quaternion.Euler(m_y, m_x, 0);
        m_transform.rotation = rotation;

        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * scrollSpeed, distanceMin, distanceMax);
        Vector3 desiredPos = rotation * new Vector3(0, 0, -distance) + playerTransform.position;

        float smoothY = Mathf.SmoothDamp(m_transform.position.y, desiredPos.y, ref velocity.y, followSpeed);
        m_transform.position = new Vector3(desiredPos.x, smoothY, desiredPos.z);

        RaycastHit hit;
        if (Physics.Raycast(playerTransform.position, (m_transform.position - playerTransform.position).normalized, out hit, (distance <= 0 ? -distance : distance)))
        {
            m_transform.position = hit.point - (m_transform.position - hit.point).normalized * 1.2f;
        }

        //TODO: send W key true to server if we hold both mouse buttons down

        //if we hold right mouse, turn character
        if (Input.GetKey(KeyCode.Mouse1))
        {
            //TODO: mayhe this needs to be server authorative? not sure
            //this is ultra getto way to prevent stuttering when changing parents rotation
            transform.parent = null;
            playerTransform.rotation = Quaternion.Euler(0, m_transform.eulerAngles.y, 0);
            transform.parent = playerTransform;
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;

        return Mathf.Clamp(angle, min, max);
    }
}