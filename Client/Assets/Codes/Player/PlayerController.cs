﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool attackToggled;
    private PlayerManager m_pm;
    private Spells m_spells;
    public bool[] playerInput = new bool[]
    {
        false,
        false,
        false,
        false,
        false
    };

    private void Awake()
    {
        m_pm = GetComponent<PlayerManager>();
        m_spells = GetComponent<Spells>();
    }

    private void Update()
    {
        UpdateInput();
        UpdateAnimations();
        SendInputToServer();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            //TODO: if casting, stop casting and dont drop target
            //also stop attack

            if(attackToggled)
            {
                attackToggled = false;
                ClientSend.ToggleAttack(attackToggled);
            }
            else
                ClientSend.UpdateTarget(-1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            attackToggled = (attackToggled) ? false : true;
            ClientSend.ToggleAttack(attackToggled);
        }

        for (int i = 0; i < InputManager.instance.keyBinds.Count; i++ )
        {
            if(Input.GetKeyDown(InputManager.instance.keyBinds[i].keybindButton))
            {
                if (m_pm.target == null)
                    ClientSend.UseSpell(InputManager.instance.keyBinds[i].spellId, -1);
                else
                    ClientSend.UseSpell(InputManager.instance.keyBinds[i].spellId, m_pm.target.id);
            }
        }

        #region oldinputcode
        /*if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if(m_pm.target == null)
                ClientSend.UseSpell(m_spells.spells[0].spellId, -1);
            else
                ClientSend.UseSpell(m_spells.spells[0].spellId, m_pm.target.id);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (m_pm.target == null)
                ClientSend.UseSpell(m_spells.spells[1].spellId, -1);
            else
                ClientSend.UseSpell(m_spells.spells[1].spellId, m_pm.target.id);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (m_pm.target == null)
                ClientSend.UseSpell(m_spells.spells[2].spellId, -1);
            else
                ClientSend.UseSpell(m_spells.spells[2].spellId, m_pm.target.id);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ClientSend.UseSpell(m_spells.spells[3].spellId, -1);
        }*/
        #endregion

    }

    private void UpdateInput()
    {
        playerInput[0] = Input.GetKey(KeyCode.W);
        playerInput[1] = Input.GetKey(KeyCode.S);
        playerInput[2] = Input.GetKey(KeyCode.A);
        playerInput[3] = Input.GetKey(KeyCode.D);
        playerInput[4] = Input.GetKey(KeyCode.Space);

        if (Input.GetKey(KeyCode.Mouse0) && Input.GetKey(KeyCode.Mouse1))
            playerInput[0] = true;
    }

    private void UpdateAnimations()
    {
        if (m_pm.anim == null)
            return;

        if (playerInput[0] || playerInput[1] || playerInput[2] || playerInput[3])
            m_pm.anim.SetFloat("Speed", 1);
        else
            m_pm.anim.SetFloat("Speed", 0);
    }

    private void SendInputToServer()
    {
        ClientSend.PlayerMovement(playerInput);
    }
}
