﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerManager : Entity
{
    public int playerId;
    public string username;
    private bool m_isLocalPlayer = false;

    protected override void Awake()
    {
        base.Awake();
        if (GetComponent<PlayerController>())
            m_isLocalPlayer = true;
    }

    protected override void OnMouseDown()
    {
        //cannot target yourself by clicking your avatar
        if (!m_isLocalPlayer)
            base.OnMouseDown();
    }

    private void Update()
    {
        if(nameplate != null)
            UpdateNameplate();
    }

    public bool IsLocalPlayer()
    {
        return m_isLocalPlayer;
    }
}
