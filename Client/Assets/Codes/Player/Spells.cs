﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spells : MonoBehaviour
{
    public List<Spell> spells = new List<Spell>();
    public bool isCasting;
    public Spell currentSpell;
    private bool m_isLocalPlayer;
    private Entity m_owner;

    private void Start()
    {
        if (GetComponent<PlayerController>())
            m_isLocalPlayer = true;

        m_owner = GetComponent<Entity>();
        currentSpell = null;

        //store the original cd values
        for(int i = 0; i < spells.Count; i++)
        {
            spells[i].spellCooldownStart = spells[i].spellCooldownMax;
        }
    }

    public void UseSpell(int spellId)
    {
        Spell s = GetSpell(spellId);
        if (s != null && !isCasting)
        {
            currentSpell = s;
            isCasting = true;

            if(m_isLocalPlayer && currentSpell.maxSpellCastProgress > 0)
            {
                UIManager.instance.playerCastBar.gameObject.SetActive(true);
                UIManager.instance.playerCastBarText.text = s.spellName;
            } 
        }
    }

    public void UseSpell(int spellId, Entity target)
    {
        Spell s = GetSpell(spellId);
        if(s != null && !isCasting)
        {
            currentSpell = s;
            isCasting = true;

            if (m_isLocalPlayer && currentSpell.maxSpellCastProgress > 0)
            {
                UIManager.instance.playerCastBar.gameObject.SetActive(true);
                UIManager.instance.playerCastBarText.text = s.spellName;
            }
               
        }
    }

    private void FixedUpdate()
    {
        if(isCasting && currentSpell != null)
        {
            UpdateSpellCasting();
        }

        if(m_isLocalPlayer)
            UpdateSpellCooldowns();
    }

    public void StopCasting(bool interrupted)
    {
        if(interrupted)
        {
            //TODO: show that we are interrupted
            
            currentSpell.spellCastProgress = 0;
            isCasting = false;
            currentSpell = null;
            if (m_isLocalPlayer)
            {
                UIManager.instance.playerCastBar.value = 0;
                UIManager.instance.playerCastBar.gameObject.SetActive(false);

                //TODO: Get this from server or hardcode the values in client before
                float interruptedCD = 4;

                for (int i = 0; i < spells.Count; i++)
                {
                    if (spells[i].spellCooldown < interruptedCD)
                    {
                        spells[i].spellCooldown = interruptedCD;
                        spells[i].spellCooldownMax = interruptedCD;
                    }
                }    
            }
        }
        else
        {
            //just stop casting normally
            if(currentSpell != null)
                currentSpell.spellCastProgress = 0;
           
            isCasting = false;
            currentSpell = null;

            if (m_isLocalPlayer)
            {
                UIManager.instance.playerCastBar.value = 0;
                UIManager.instance.playerCastBar.gameObject.SetActive(false);
            }
        }
    }

    private void UpdateSpellCasting()
    {
        currentSpell.spellCastProgress += Time.deltaTime;
        if (m_isLocalPlayer)
            UIManager.instance.playerCastBar.value = currentSpell.spellCastProgress / currentSpell.maxSpellCastProgress;

        if (currentSpell.spellCastProgress >= currentSpell.maxSpellCastProgress)
        {
            //currentSpell.spellCastProgress = currentSpell.maxSpellCastProgress;
            //incase we never receive stopcasting packet from server, finish the cast, stop the castbar
            StopCasting(false);
        }
    }

    private void UpdateSpellCooldowns()
    {
        for (int i = 0; i < spells.Count; i++)
        {
            if (spells[i].spellCooldown > 0)
                spells[i].spellCooldown -= Time.deltaTime;

            if (spells[i].spellCooldown < 0)
            {
                spells[i].spellCooldown = 0;
                spells[i].spellCooldownMax = spells[i].spellCooldownStart;
            }
        }
    }

    //cast finished (self cast)
    public void CastFinished(int spellId)
    {
        if (m_isLocalPlayer)
        {
            UIManager.instance.playerCastBar.gameObject.SetActive(false);
            UIManager.instance.playerCastBarText.text = "";
        }

        //TODO: Instantiate grafix for the spell or toggle animation or something

        //add buff if there is id specified
        if (currentSpell != null && currentSpell.buffId > 0)
        {
            GetComponent<Buffs>().AddBuff(GameManager.GM.GetBuff(currentSpell.buffId), false);
        }

        if (currentSpell != null && currentSpell.spellId == spellId)
        {
            currentSpell.spellCooldown = currentSpell.spellCooldownMax;
            currentSpell.spellCastProgress = 0;
            currentSpell = null;
            isCasting = false;
        }
    }

    public void CastFinished(int spellId, Entity targetId)
    {
        if (m_isLocalPlayer)
        {
            UIManager.instance.playerCastBar.gameObject.SetActive(false);
            UIManager.instance.playerCastBarText.text = "";
        }

        //TODO: Instantiate grafix for the spell or toggle animation or something
        //here we can shoot a ball between user and target for looks

        //TODO: Fix this. It bugs out when you change target before cast finishes
        //obv it works fine on server, just the client UI bugs out
        if (currentSpell != null && currentSpell.buffId > 0)
        {
            if (targetId != null && !currentSpell.friendlySpell)
            {
                targetId.GetComponent<Buffs>().AddBuff(GameManager.GM.GetBuff(currentSpell.buffId), true);
            }
            else
            {
                GetComponent<Buffs>().AddBuff(GameManager.GM.GetBuff(currentSpell.buffId), false);
            }
        }

        if (currentSpell != null && currentSpell.spellId == spellId)
        {
            currentSpell.spellCooldown = currentSpell.spellCooldownMax;
            currentSpell.spellCastProgress = 0;
            currentSpell = null;
            isCasting = false;
        }
    }

    public Spell GetSpell(int spellId)
    {
        for (int i = 0; i < spells.Count; i++)
        {
            if (spells[i].spellId == spellId)
                return spells[i];
        }

        return null;
    }
}

[System.Serializable]
public class Spell
{
    public string spellName;
    public int spellId;
    public float spellCastProgress;
    public float maxSpellCastProgress;
    public float spellCooldown;
    public float spellCooldownMax;
    public float spellCooldownStart;
    public bool friendlySpell;
    public int buffId;
}
