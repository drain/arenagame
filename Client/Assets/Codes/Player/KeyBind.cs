﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class KeyBind
{
    public KeyCode keybindButton;
    public int spellId;
    public Image button;
    public Sprite icon;
    public string keybindString;
    public Text keybindText;
}
