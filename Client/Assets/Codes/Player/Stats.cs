﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Buffs))]
public class Stats : MonoBehaviour
{
    public float health;
    public float maxHealth;

    public float mana;
    public float maxMana;

    public float attackSpeed;
}

public enum DamageSource
{
    AutoAttack,
    Spell,
    DoT,
    Environment
}

