﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buffs : MonoBehaviour
{
    public List<Buff> buffs = new List<Buff>();
    public List<Buff> debuffs = new List<Buff>();

    private void Update()
    {
        if (buffs.Count > 0)
        {
            //update buffs
            UpdateBuffTimers(buffs);
        }

        if (debuffs.Count > 0)
        {
            //update debuffs
            UpdateBuffTimers(debuffs);
        }
    }

    public void AddBuff(Buff b, bool isTarget)
    {
        Buff tempBuff = new Buff();
        tempBuff.Copy(b);

        buffs.Add(tempBuff);

        if(isTarget)
        {
            UIManager.instance.AddTargetBuff(tempBuff);
        }
        else
        {
            UIManager.instance.AddPlayerBuff(tempBuff);
        }

    }

    public void AddDebuff(Buff db)
    {
        Buff tempBuff = new Buff();
        tempBuff.Copy(db);

        debuffs.Add(tempBuff);

        if (GetComponent<PlayerManager>() && GetComponent<PlayerManager>().IsLocalPlayer())
            UIManager.instance.AddPlayerBuff(tempBuff);
    }

    public void UpdateBuffTimers(List<Buff> bs)
    {
        for(int i = bs.Count-1; i >= 0; i--)
        {
            if (bs[i].timerRemaining > 0)
            {
                bs[i].timerRemaining -= Time.deltaTime;
                UIManager.instance.UpdateBuffText(bs[i]);
            } 
            else
            {
                UIManager.instance.RemoveBuff(bs[i]);
                bs.RemoveAt(i);
            }
        }
    }
}
