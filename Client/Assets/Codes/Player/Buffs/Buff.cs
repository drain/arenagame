﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff
{
    public int id;
    public string name;
    public string desc;
    public Sprite image;
    public float timerMax;
    public float timerRemaining;
    public bool isDebuff;
    public GameObject uiItem;

    public void Copy(Buff values)
    {
        id = values.id;
        name = values.name;
        desc = values.desc;
        image = values.image;
        timerMax = values.timerMax;
        timerRemaining = values.timerRemaining;
        isDebuff = values.isDebuff;
    }
}
