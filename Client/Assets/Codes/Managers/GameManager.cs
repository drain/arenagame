﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;

    public static Dictionary<int, PlayerManager> players = new Dictionary<int, PlayerManager>();

    public GameObject localPlayerPrefab;
    public GameObject playerPrefab;

    public PlayerManager localPlayer;

    public List<Buff> allBuffs = new List<Buff>();

    public Buff GetBuff(int id)
    {
        for(int i = 0; i < allBuffs.Count; i++)
        {
            if (allBuffs[i].id == id)
                return allBuffs[i];
        }
        return null;
    }

    private void Awake()
    {
        GM = this;
    }

    public void SpawnPlayer(int id, int entityId, string username, Vector3 position, Quaternion rotation)
    {
        GameObject player;

        if(id == Client.instance.myId)
        {
            player = Instantiate(localPlayerPrefab, position, rotation);

            //close menu
            FindObjectOfType<UIManager>().startMenu.SetActive(false);
            UIManager.instance.playerUIName.text = username;
            localPlayer = player.GetComponent<PlayerManager>();
            UIManager.instance.isInMainMenu = false;
        }
        else
        {
            player = Instantiate(playerPrefab, position, rotation);
        }

        PlayerManager pm = player.GetComponent<PlayerManager>();

        pm.playerId = id;
        pm.id = entityId;
        pm.username = username;
        pm.eName = username;
        pm.nameText.text = pm.username;

        players.Add(id, pm);
        EntityManager.entities.Add(pm.id, pm);
    }
}
