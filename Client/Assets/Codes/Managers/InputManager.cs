﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;
    public List<KeyBind> keyBinds = new List<KeyBind>();

    private void Awake()
    {
        instance = GetComponent<InputManager>();
    }

    private void Start()
    {
        SetKeybinds();
    }

    public void SetKeybinds()
    {
        for (int i = 0; i < keyBinds.Count; i++)
        {
            if(keyBinds[i].button != null)
                keyBinds[i].button.sprite = keyBinds[i].icon;
            if(keyBinds[i].keybindText != null)
                keyBinds[i].keybindText.text = keyBinds[i].keybindString;
        }
    }
}
