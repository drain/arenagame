﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject startMenu;
    public InputField userNameField;
    public InputField serverIP;

    public bool isInMainMenu = true;

    public Canvas playerUI;

    //player frame
    public Text playerUIName;
    public Slider playerHpBar;
    public Text playerHpText;
    public Slider playerManaBar;
    public Text playerManaText;
    public Slider playerCastBar;
    public Text playerCastBarText;

    //target frame
    public Text playerUITargetName;
    public Slider targetHpBar;
    public Text targetHpText;
    public Slider targetManaBar;
    public Text targetManaText;
    public Slider targetCastBar;
    public Text targetCastBarText;

    //target of target frame
    public Text playerUITargetOfTargetName;
    public Slider targetOfTargetHpBar;
    public Slider targetOfTargetManaBar;

    public Text textLog;
    public int maxLines;

    public GameObject menuCamera;

    public TextMeshPro combatTextPrefab;

    public GameObject playerBuffArea;
    public GameObject targetBuffArea;
    public GameObject buffPrefab;

    private void Awake()
    {
        instance = this;
    }

    private void FixedUpdate()
    {
        if(!isInMainMenu)
        {
            UpdateHealthBars();
            UpdateTargetCastBar();
            UpdateAbilityCooldowns();
        }
    }

    public void UpdateHealthBars()
    {
        //update our healthbar
        Stats playerStats = GameManager.GM.localPlayer.stats;
        if(playerStats.maxHealth > 0)
        {
            playerHpBar.value = playerStats.health / playerStats.maxHealth;
            playerManaBar.value = playerStats.mana / playerStats.maxMana;
            playerHpText.text = playerStats.health + "/" + playerStats.maxHealth;
            playerManaText.text = playerStats.mana + "/" + playerStats.maxMana;
        }

        //our targets healthbar
        if (GameManager.GM.localPlayer.target != null)
        {
            Stats targetStats = GameManager.GM.localPlayer.target.stats;
            targetHpBar.value = targetStats.health / targetStats.maxHealth;
            targetManaBar.value = targetStats.mana / targetStats.maxMana;
            targetHpText.text = targetStats.health + "/" + targetStats.maxHealth;
            targetManaText.text = targetStats.mana + "/" + targetStats.maxMana;

            //our targets targets healthbar
            if (GameManager.GM.localPlayer.target.target != null)
            {
                Stats targetOfTargetStats = GameManager.GM.localPlayer.target.target.stats;
                targetOfTargetHpBar.value = targetOfTargetStats.health / targetOfTargetStats.maxHealth;
                //targetOfTargetManaBar.value = targetOfTargetStats.mana / targetOfTargetStats.maxMana;
            }
        }

        //for ingame healthbars
        //healthBar.transform.LookAt(healthBar.transform.position + m_cam.transform.rotation * Vector3.back,
        //m_cam.transform.rotation* Vector3.down);
    }

    public void UpdateTargetCastBar()
    {
        if (GameManager.GM.localPlayer.target != null)
        {
            Spells spells = GameManager.GM.localPlayer.target.spells;
           
            if(spells.currentSpell != null && spells.currentSpell.maxSpellCastProgress > 0)
            {
                if (!targetCastBar.gameObject.activeSelf)
                {
                    targetCastBar.gameObject.SetActive(true);
                    targetCastBarText.text = spells.currentSpell.spellName;
                }

                targetCastBar.value = spells.currentSpell.spellCastProgress / spells.currentSpell.maxSpellCastProgress;
            }
            else
            {
                if (targetCastBar.gameObject.activeSelf)
                {
                    targetCastBarText.text = "";
                    targetCastBar.gameObject.SetActive(false);
                }
                    
            }
        }

    }

    public void UpdateTargetUI()
    {
        if (GameManager.GM.localPlayer.target != null)
        {
            playerUITargetName.transform.parent.gameObject.SetActive(true);
            playerUITargetName.text = GameManager.GM.localPlayer.target.eName;

            UpdateTargetBuffs();
            //check for target of target
            if (GameManager.GM.localPlayer.target.target != null)
            {
                playerUITargetOfTargetName.transform.parent.gameObject.SetActive(true);
                playerUITargetOfTargetName.text = GameManager.GM.localPlayer.target.target.eName;

            }
            else
            {
                playerUITargetOfTargetName.transform.parent.gameObject.SetActive(false);
            }
        }
        else
        {
            //TODO: this better
            playerUITargetName.transform.parent.gameObject.SetActive(false);
        }
    }

    public void UpdateAbilityCooldowns()
    {
        for(int i = 0; i < InputManager.instance.keyBinds.Count; i++)
        {
            KeyBind kb = InputManager.instance.keyBinds[i];
            Spell spell = GameManager.GM.localPlayer.spells.GetSpell(kb.spellId);

            if(kb.button != null)
                kb.button.fillAmount = 1 - (spell.spellCooldown / spell.spellCooldownMax);
        }
    }

    public void AddPlayerBuff(Buff buff)
    {
        GameObject b = Instantiate(buffPrefab, playerBuffArea.transform);
        buff.uiItem = b;
        b.GetComponent<Image>().sprite = buff.image;
        b.GetComponentInChildren<Text>().text = buff.timerRemaining.ToString();
    }

    public void RemoveBuff(Buff buff)
    {
        if(buff.uiItem != null)
        {
            buff.uiItem.transform.parent = null;
            Destroy(buff.uiItem);
        }
    }

    public void UpdateTargetBuffs()
    {
        //clear all existing buff icons, (save first since its the holding object)
        Transform[] children = targetBuffArea.GetComponentsInChildren<Transform>();
        for(int i = children.Length-1; i > 0; i--)
        {
            Destroy(children[i].gameObject);
        }

        Buffs buffs = GameManager.GM.localPlayer.target.GetComponent<Buffs>();

        for(int i = 0; i < buffs.buffs.Count; i++)
        {
            AddTargetBuff(buffs.buffs[i]);
        }
    }

    public void AddTargetBuff(Buff buff)
    {
        GameObject b = Instantiate(buffPrefab, targetBuffArea.transform);
        buff.uiItem = b;
        b.GetComponent<Image>().sprite = buff.image;
        b.GetComponentInChildren<Text>().text = buff.timerRemaining.ToString();
    }

    public void RemoveTargetBuff(Buff buff)
    {
        buff.uiItem.transform.parent = null;
        Destroy(buff.uiItem);
    }

    public void UpdateBuffText(Buff b)
    {
        if(b.uiItem != null)
            b.uiItem.GetComponentInChildren<Text>().text = b.timerRemaining.ToString("F0");
    }

    public void SpawnAutoAttackNumber(Entity target, float value)
    {
        GameObject go = Instantiate(combatTextPrefab.gameObject, target.transform.position, Quaternion.identity);
        go.GetComponent<CombatText>().Initialize("-" + value.ToString("F0"), Color.white, .2f, (Random.Range(0,2) == 0) ? true : false);
    }

    public void SpawnSpellDamageNumber(Entity target, float value)
    {
        GameObject go = Instantiate(combatTextPrefab.gameObject, target.transform.position, Quaternion.identity);
        go.GetComponent<CombatText>().Initialize("-" + value.ToString("F0"), Color.yellow, .2f, (Random.Range(0, 2) == 0) ? true : false);
    }

    public void SpawnDamageTakenNumber(Entity target, float value)
    {
        GameObject go = Instantiate(combatTextPrefab.gameObject, target.transform.position, Quaternion.identity);
        go.GetComponent<CombatText>().Initialize("-" + value.ToString("F0"), Color.red, .2f, (Random.Range(0, 2) == 0) ? true : false);
    }

    public void SpawnHealNumber(Entity target, float value)
    {
        GameObject go = Instantiate(combatTextPrefab.gameObject, target.transform.position, Quaternion.identity);
        go.GetComponent<CombatText>().Initialize("+" + value.ToString("F0"), Color.green, .2f, (Random.Range(0, 2) == 0) ? true : false);
    }

    ///Prints text into new row on the log and deletes old lines if linecount is > "maxLines"
    public static void WriteToLog(string text)
    {
        string[] rows = instance.textLog.text.Split('\n');
        if (rows.Length > instance.maxLines)
        {
            for (int i = 0; i < rows.Length; i++)
                rows[i] = (i == rows.Length - 1) ? text : rows[i + 1];

            instance.textLog.text = "";

            for (int i = 0; i < rows.Length; i++)
                instance.textLog.text += (i == rows.Length - 1) ? rows[i] : rows[i] + '\n';
        }
        else
        {
            instance.textLog.text += "\n";
            instance.textLog.text += text;
        }
    }



}
