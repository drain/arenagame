﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CombatText : MonoBehaviour
{
    private TextMeshPro m_textMesh;
    private Camera m_playerCam;
    private float m_curDuration;
    private float m_maxDuration;
    private Color m_color;
    private Vector3 m_moveVector;

    private void Awake()
    {
        m_textMesh = GetComponent<TextMeshPro>();
        m_playerCam = FindObjectOfType<Camera>();
    }

    public void Initialize(string value, Color col, float duration, bool isCrit)
    {
        m_textMesh.text = value;
        m_textMesh.color = col;
        m_color = m_textMesh.color;
        m_maxDuration = duration;
        m_curDuration = m_maxDuration;

        //int dir = (Random.Range(0, 2) == 0) ? 1 : -1;

        if (isCrit)
        {
            m_textMesh.fontSize = 18;
            m_moveVector = new Vector3(Random.Range(-.7f, .7f), 1, 0) * 10;
        }
        else
        {
            m_textMesh.fontSize = 10;
            m_moveVector = new Vector3(Random.Range(-.7f, .7f), 1, 0) * 5;
        }
    }

    private void Update()
    {
        UpdateLookAt();

        if (m_curDuration >= 0)
            m_curDuration -= Time.deltaTime;
        else
            FadeOut();
    }
    
    public void FadeOut()
    {
        float disSpeed = .5f;
        m_color.a -= disSpeed * Time.deltaTime;
        m_textMesh.color = m_color;
        if (m_color.a < 0)
            Destroy(gameObject);

    }

    public void UpdateLookAt()
    {
        if(m_curDuration > 0)
            transform.position += m_moveVector * Time.deltaTime;

        transform.LookAt(transform.position + m_playerCam.transform.rotation * Vector3.forward,
                                       m_playerCam.transform.rotation * Vector3.up);

        if(m_curDuration > (m_maxDuration / 2))
        {
            float increaseScale = 2;
            transform.localScale += Vector3.one * increaseScale * Time.deltaTime;
        }
        else if(m_curDuration > 0)
        {
            float increaseScale = 2;
            transform.localScale -= Vector3.one * increaseScale * Time.deltaTime;
        }

        /*float dist = Vector3.Distance(transform.position, m_playerCam.transform.position);

        if (dist > 35)
            nameplate.gameObject.SetActive(false);
        else
        {
            if (!nameplate.gameObject.activeSelf)
                nameplate.gameObject.SetActive(true);

            nameplate.transform.localScale = new Vector3(0.015f, 0.015f, 1) * dist / 13.5f;
        }*/

    }

}
