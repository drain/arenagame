﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArenaGameServer
{
    class GameLogic
    {
        public static void Update()
        {
            foreach(Client c in Server.clients.Values)
            {
                if(c.player != null)
                {
                    c.player.Update();
                }
            }

            ThreadManager.UpdateMain();
        }
    }
}
