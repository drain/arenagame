﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArenaGameServer
{
    class Program
    {
        private static bool isRunning = false;

        static void Main(string[] args)
        {
            Console.Title = "Arena Game Server";
            isRunning = true;

            Thread mainThread = new Thread(new ThreadStart(MainThread));
            mainThread.Start();

            Server.Start(10, 55000);
        }

        private static void MainThread()
        {
            Console.WriteLine("Main thread started. Running at " + Constants.TICKS_PER_SEC + " ticks per second.");
            DateTime nextLoop = DateTime.Now;

            //this is gonna need some deltatime shit for sure
            while(isRunning)
            {
                while(nextLoop < DateTime.Now)
                {
                    GameLogic.Update();

                    nextLoop = nextLoop.AddMilliseconds(Constants.MS_PER_TICK);

                    //sleep untill next loop to reduce CPU uptime
                    if(nextLoop > DateTime.Now)
                    {
                        Thread.Sleep(nextLoop - DateTime.Now);
                    }
                }
            }

        }
    }
}
