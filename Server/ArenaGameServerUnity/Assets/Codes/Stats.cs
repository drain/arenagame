﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float health;
    public float maxHealth;

    public float mana;
    public float maxMana;

    public float attackSpeed;
    public float hpRegen;
    public float manaRegen;

    private float m_regenTimer;
    public float regenTimerMax;

    public bool isDead;

    public bool isDirty;

    //TODO: armor, resistances, etc.

    public void Heal(float heal)
    {
        if (isDead)
            return;

        health += heal;
        if (health > maxHealth)
            health = maxHealth;
        isDirty = true;
    }

    public void TakeDamage(float damage)
    {
        if (isDead)
            return;

        health -= damage;
        if(health <= 0)
            Dead();
        isDirty = true;
    }

    /*public bool UseMana(int cost)
    {
        if (mana >= cost)
        {
            mana -= cost;
            return true;
        }
        else
            return false;
    }*/

    public void PassiveRegen()
    {
        if (isDead)
            return;

        if (m_regenTimer < regenTimerMax)
            m_regenTimer += Time.deltaTime;
        else
        {
            m_regenTimer = 0;

            if (health < maxHealth)
                health += hpRegen;
            if (health >= maxHealth)
                health = maxHealth;

            if (mana < maxMana)
                mana += manaRegen;
            if (mana >= maxMana)
                mana = maxMana;

            isDirty = true;
        }
    }

    public void Dead()
    {
        //handle death
        //mark it as dead
        //wait for death animation or something

        isDead = true;

        if (GetComponent<Player>())
        {
            ThreadManager.ExecuteOnMainThread(() =>
            {
                //TODO: Implement respawn or something
                GetComponent<Player>().canMove = false;
                GetComponent<AutoAttack>().canAttack = false;
            });
        }
        else if (GetComponent<NPC>())
        {
            ThreadManager.ExecuteOnMainThread(() =>
            {
                int entityId = GetComponent<Entity>().id;

                foreach (Entity e in EntityManager.instance.entities)
                    if (e.target != null && e.target.id == entityId)
                    {
                        e.target = null;
                        ServerSend.UpdateTarget(e.id, -1);
                    }
                
                EntityManager.instance.RemoveEntity(entityId);
                ServerSend.EntityRemove(entityId);

                Destroy(this.gameObject);
            });
        }
    }
}

public enum DamageSource
{
    AutoAttack,
    Spell,
    DoT,
    Environment
}
