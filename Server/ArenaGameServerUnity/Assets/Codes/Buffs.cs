﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buffs : MonoBehaviour
{
    public List<BuffSO> allBuffs = new List<BuffSO>();

    public List<Buff> currentBuffs = new List<Buff>();
    public List<Buff> currentDebuffs = new List<Buff>();

    public int buffLimit;
    public int debuffLimit;

    private Entity m_owner;

    private void Awake()
    {
        m_owner = GetComponent<Entity>();
    }

    private void FixedUpdate()
    {
        UpdateBuffs();
        UpdateDebuffs();
    }

    public void UpdateBuffs()
    {
        for (int i = currentBuffs.Count -1; i >= 0; i--)
        {
            currentBuffs[i].UpdateBuffTimer();
            if(currentBuffs[i].curDuration <= 0)
            {
                currentBuffs[i].RemoveBuff(m_owner);
                currentBuffs.RemoveAt(i);
            }
        }
    }

    public void UpdateDebuffs()
    {
        for (int i = currentDebuffs.Count -1; i >= 0 ; i--)
        {
            currentDebuffs[i].UpdateBuffTimer();
            if (currentDebuffs[i].curDuration <= 0)
            {
                currentDebuffs[i].RemoveBuff(m_owner);
                currentDebuffs.RemoveAt(i);
            }
        }
    }

    public void ApplyBuff(Buff buff, Entity buffUser)
    {
        Buff b = null;
        for(int i = 0; i < currentBuffs.Count; i++)
        {
            //its the same buff, but it cannot stack
            if(currentBuffs[i].buffId == buff.buffId && !currentBuffs[i].canBeStacked)
            {
                b = currentBuffs[i];
                return;
            }

            //if it has the same user as the same buff before
            if(currentBuffs[i].buffId == buff.buffId && currentBuffs[i].userId == buffUser.id)
            {
                b = currentBuffs[i];
                break;
            }
        }

        //its same spell given by same player, we dont add new buff, instead we set time to max
        //or its a buff that cannot stack multiple times
        if(b != null)
        {
            b.curDuration = b.duration;
            return;
        }
        else
        {
            //add new buff since we dont have it
            //if it fits
            if (currentBuffs.Count < buffLimit)
            {
                buff.userId = buffUser.id;
                currentBuffs.Add(buff);
                buff.ApplyBuff(m_owner);
            }
        }
    }

    public void ApplyDebuff(Buff buff, Entity debuffUser)
    {
        Buff b = null;
        for (int i = 0; i < currentDebuffs.Count; i++)
        {
            //its the same buff, but it cannot stack
            if (currentDebuffs[i].buffId == buff.buffId && !currentDebuffs[i].canBeStacked)
            {
                b = currentDebuffs[i];
                return;
            }

            //if it has the same user as the same buff before
            if (currentDebuffs[i].buffId == buff.buffId && currentDebuffs[i].userId == debuffUser.id)
            {
                b = currentDebuffs[i];
                break;
            }
        }

        //its same spell given by same player, we dont add new buff, instead we set time to max
        //or its a buff that cannot stack multiple times
        if (b != null)
        {
            b.curDuration = b.duration;
            return;
        }
        else
        {
            //add new debuff since we dont have it
            //if it fits
            if (currentDebuffs.Count < debuffLimit)
            {
                buff.userId = debuffUser.id;
                currentDebuffs.Add(buff);
                buff.ApplyBuff(m_owner);
            }
        }
       
    }

    public BuffSO GetBuffByID(int buffId)
    {
        for(int i = 0; i < allBuffs.Count; i++)
        {
            if(buffId == allBuffs[i].buffId)
            {
                return allBuffs[i];
            }
        }

        return null;
    }

    public Buff CreateBuff(BuffSO buffSO)
    {
        Buff buff;
        switch (buffSO.buffType)
        {
            case BuffType.None:
                return new Buff();
            case BuffType.HotHeal:
                buff = new HotHeal();
                buff.Initialize(buffSO);
                return buff;
            case BuffType.Statbuff:
                buff = new Statbuff();
                buff.Initialize(buffSO);
                return buff;
            case BuffType.DotDamage:
                buff = new DotDamage();
                buff.Initialize(buffSO);
                return buff;
            case BuffType.StatDebuff:
                buff = new StatDebuff();
                buff.Initialize(buffSO);
                return buff;
            default:
                return new Buff();
        }
    }

}
