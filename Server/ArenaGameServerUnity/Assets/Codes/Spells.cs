﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spells : MonoBehaviour
{
    public List<SpellSO> spellPrefabs = new List<SpellSO>();
    public List<Spell> spells = new List<Spell>();

    //TODO: Add global cooldown, or maybe not needed.. not sure yet

    public bool isCasting;
    public Spell currentCastedSpell;
    public Entity target;

    private Entity m_owner;
    private Stats m_ownerStats;

    private Vector3 m_castStartPos;
    public LayerMask losMask;

    private void Awake()
    {
        //populate player spells using SOs
        for(int i = 0; i < spellPrefabs.Count; i++)
        {
            switch(spellPrefabs[i].spellId)
            {
                case 1:
                    DamageSpell fireball = new DamageSpell();
                    fireball.Initialize(spellPrefabs[i]);
                    spells.Add(fireball);
                    break;

                case 2:
                    DamageSpell strike = new DamageSpell();
                    strike.Initialize(spellPrefabs[i]);
                    spells.Add(strike);
                    break;

                case 3:
                    Interrupt interrupt = new Interrupt();
                    interrupt.Initialize(spellPrefabs[i]);
                    spells.Add(interrupt);
                    break;

                case 4:
                    Heal heal = new Heal();
                    heal.Initialize(spellPrefabs[i]);
                    spells.Add(heal);
                    break;

                case 5:
                    ApplyBuffSpell reju = new ApplyBuffSpell();
                    reju.Initialize(spellPrefabs[i]);
                    spells.Add(reju);
                    break;

                case 6:
                    ApplyBuffSpell enrage = new ApplyBuffSpell();
                    enrage.Initialize(spellPrefabs[i]);
                    spells.Add(enrage);
                    break;

                case 7:
                    ApplyDebuffSpell curse = new ApplyDebuffSpell();
                    curse.Initialize(spellPrefabs[i]);
                    spells.Add(curse);
                    break;

                case 8:
                    ApplyBuffSpell fortitude = new ApplyBuffSpell();
                    fortitude.Initialize(spellPrefabs[i]);
                    spells.Add(fortitude);
                    break;

                case 9:
                    ApplyDebuffSpell weakness = new ApplyDebuffSpell();
                    weakness.Initialize(spellPrefabs[i]);
                    spells.Add(weakness);
                    break;

                default:
                    Spell s = new Spell();
                    s.Initialize(spellPrefabs[i]);
                    spells.Add(s);
                    break;   
            }
        }
    }

    private void Start()
    {
        m_owner = GetComponent<Entity>();
        m_ownerStats = GetComponent<Stats>();
    }

    public Spell GetSpellById(int id)
    {
        for (int i = 0; i < spells.Count; i++)
            if (spells[i].spellId == id)
                return spells[i];

        return null;
    }

    public bool CanBeCasted(Spell spell)
    {
        if (!isCasting && spell != null && spell.cooldown <= 0 && m_ownerStats.mana >= spell.manaCost)
        {
            return true;
        }

        return false;
    }

    //casted without target
    public void UseSpell(Spell spell)
    {
        if (!CanBeCasted(spell) || m_ownerStats.isDead)
            return;

        spell.castProgress = 0;
        isCasting = true;
        currentCastedSpell = spell;
        m_castStartPos = transform.position;

        ServerSend.UseSpell(m_owner, spell.spellId);
    }

    //casted to target
    public void UseSpell(Spell spell, Entity target)
    {
        //start casting
        //---- cast untill moved, interrupted, cancelled or cast finished
        //do cast effect -> set cooldown to max

        if (!CanBeCasted(spell) || m_ownerStats.isDead)
            return;
        
        if (!spell.requiresTarget || (spell.requiresTarget && target != null))
        {
            //TODO: check the angle that we are facing the target (we would need to make rotation server auth if we want this)
            if (IsInLOS(target, spell.range) && !target.stats.isDead)
            {
                spell.castProgress = 0;
                isCasting = true;
                currentCastedSpell = spell;
                this.target = target;
                m_castStartPos = transform.position;

                ServerSend.UseSpell(m_owner, spell.spellId);
            }
          
        }
    }

    public bool IsInLOS(Entity target, float range)
    {
        if (target == null)
            return false;

        RaycastHit[] hits = Physics.RaycastAll(transform.position, target.transform.position - transform.position, range, losMask);
        //hits.OrderByDescending(c => c.distance);
        System.Array.Sort(hits, (x, y) => x.distance.CompareTo(y.distance));

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider != null && !hits[i].collider.GetComponent<Entity>())
            {
                return false;
            }
            else if (hits[i].collider != null && hits[i].collider.GetComponent<Entity>()
                && hits[i].collider.GetComponent<Entity>().id == target.id)
            {
                return true;
            }
        }

        return false;
    }

    public void UpdateSpellCastTime()
    {
        if (isCasting)
        {
            if(currentCastedSpell != null)
            {
                if(m_castStartPos != transform.position)
                {
                    //TODO: cancel cast
                    StopCasting();
                    ServerSend.CastCancelled(m_owner, false);
                    return;
                }

                currentCastedSpell.castProgress += Time.deltaTime;

                //cast finished
                if(currentCastedSpell.castProgress >= currentCastedSpell.castSpeed)
                {
                    CastFinished();
                }
            }
        }
    }

    public void StopCasting()
    {
        if(isCasting)
        {
            currentCastedSpell.castProgress = 0;
            isCasting = false;
            currentCastedSpell = null;
            target = null;
            m_castStartPos = Vector3.zero;
        }
    }

    public void Interrupt(float lockDuration)
    {
        if (isCasting)
        {
            StopCasting();
            UpdateSpellCooldowns(lockDuration);
            ServerSend.CastCancelled(m_owner, true);
        }
    }

    public void CastFinished()
    {
        if (currentCastedSpell == null)
            return;

        if (currentCastedSpell.requiresTarget)
        {
            //TODO: check the angle that we are facing the target (we would need to make rotation server auth if we want this)
            //Cast on last target (we need to track last target)
            if (IsInLOS(target,currentCastedSpell.range))
            {
                //float v = currentCastedSpell.value;
                currentCastedSpell.UseSpell(m_owner, target);
                ServerSend.CastFinished(m_owner, currentCastedSpell.spellId, currentCastedSpell.value);
            }
        }
        else
        {
            currentCastedSpell.UseSpell(m_owner);
            ServerSend.CastFinished(m_owner, currentCastedSpell.spellId, currentCastedSpell.value);
        }

        currentCastedSpell.castProgress = 0;
        isCasting = false;
        currentCastedSpell = null;
        target = null;
        m_castStartPos = Vector3.zero;
    }

    public void UpdateSpellCooldowns()
    {
        for(int i = 0; i < spells.Count; i++)
        {
            if (spells[i].cooldown > 0)
                spells[i].cooldown -= Time.deltaTime;
        }
    }

    public void UpdateSpellCooldowns(float value)
    {
        for (int i = 0; i < spells.Count; i++)
        {
            if(spells[i].cooldown < value)
                spells[i].cooldown = value;
        }
    }

    private void FixedUpdate()
    {
        //ThreadManager.ExecuteOnMainThread(() =>
        //{
        UpdateSpellCastTime();
        UpdateSpellCooldowns();
        //});
    }

}
