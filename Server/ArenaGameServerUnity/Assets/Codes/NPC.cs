﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Entity
{
    public int prefabId;
    public CharacterController controller;
    public float gravity = -9.81f;
    public float moveSpeed = 5f;
    public float jumpSpeed = 5f;

    private float yVelocity = 0;
    public bool canMove;

    private Vector3 lastPos = Vector3.zero;
    private Quaternion curRot = Quaternion.identity;

    protected override void Awake()
    {
        base.Awake();
        canMove = true;
    }

    protected override void Start()
    {
        base.Start();

        gravity *= Time.fixedDeltaTime * Time.fixedDeltaTime;
        moveSpeed *= Time.fixedDeltaTime;
        jumpSpeed *= Time.fixedDeltaTime;
    }

    private void FixedUpdate()
    {
        if (stats.isDirty)
        {
            ServerSend.UpdateStats(this);
            stats.isDirty = false;
        }

        if (canMove)
            Move(Vector2.zero);
    }

    private void Move(Vector2 move)
    {
        Vector3 moveDir = transform.right * move.x + transform.forward * move.y;
        moveDir *= moveSpeed;
        yVelocity += gravity;

        moveDir.y = yVelocity;

        controller.Move(moveDir);

        if (lastPos != transform.position)
        {
            lastPos = transform.position;
            ServerSend.EntityPosition(this);
        }

        if(curRot != transform.rotation)
        {
            curRot = transform.rotation;
            ServerSend.EntityRotation(this);
        }
    }
}
