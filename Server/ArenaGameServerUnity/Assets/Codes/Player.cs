﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity
{
    public int playerId;
    public string username;
    public CharacterController controller;
    public float gravity = -9.81f;
    public float moveSpeed = 5f;
    public float jumpSpeed = 5f;

    private bool[] inputs;
    private float yVelocity = 0;

    public bool canMove;

    private Vector3 lastPos = Vector3.zero;

    protected override void Awake()
    {
        base.Awake();
        canMove = true;
    }

    protected override void Start()
    {
        base.Start();

        gravity *= Time.fixedDeltaTime * Time.fixedDeltaTime;
        moveSpeed *= Time.fixedDeltaTime;
        jumpSpeed *= Time.fixedDeltaTime;
    }

    public void Initialize(int playerId, string username, Vector3 spawnPos)
    {
        this.playerId = playerId;
        this.username = username;
        transform.position = spawnPos;
        inputs = new bool[5];
    }

    public void SetInput(bool[] inputs, Quaternion rotation)
    {
        this.inputs = inputs;
        transform.rotation = rotation;
        //this.rotation = rotation;
    }

    //update player on server
    public void FixedUpdate()
    {
        Vector2 inputDirection = Vector2.zero;
        if (inputs[0])
        {
            inputDirection.y += 1;
        }
        if (inputs[1])
        {
            inputDirection.y -= 1;
        }
        if (inputs[2])
        {
            inputDirection.x -= 1;
        }
        if (inputs[3])
        {
            inputDirection.x += 1;
        }

        if(canMove)
            Move(inputDirection);

        stats.PassiveRegen();

        if(stats.isDirty)
        {
            ServerSend.UpdateStats(this);
            stats.isDirty = false;
        }  
    }

    private void Move(Vector2 inputDirection)
    {
        //Vector3 forward = Vector3.Transform(new Vector3(0, 0, 1), rotation);
        //Vector3 right = Vector3.Normalize(Vector3.Cross(forward, new Vector3(0, 1, 0)));

        Vector3 moveDir = transform.right * inputDirection.x + transform.forward * inputDirection.y;
        moveDir *= moveSpeed;

        if(controller.isGrounded)
        {
            yVelocity = 0;
            //jump key
            if(inputs[4])
            {
                yVelocity = jumpSpeed;
            }
        }
        yVelocity += gravity;

        moveDir.y = yVelocity;
        controller.Move(moveDir);

        //transform.position += moveDir * moveSpeed;

        //only update position if that player actually moved
        //needs some more work :)
        /*if(lastPos != transform.position)
        {
            lastPos = transform.position;
        }*/

        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }
    
}
