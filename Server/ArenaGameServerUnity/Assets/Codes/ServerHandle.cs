﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class ServerHandle
{
    public static void WelcomeReceived(int fromClient, Packet packet)
    {
        int clientIdCheck = packet.ReadInt();
        string username = packet.ReadString();

        Debug.Log(Server.clients[fromClient].tcp.socket.Client.RemoteEndPoint + " connected successfully and is now player " + fromClient + " (username:" + username + ")");
        if (fromClient != clientIdCheck)
        {
            Debug.Log("Player " + username + " ID: " + fromClient + " has assumed the wrong client ID + " + clientIdCheck);
        }

        Server.clients[fromClient].SendIntoGame(username);
    }

    public static void CharacterLoaded(int fromClient, Packet packet)
    {
        /*
            Update all buff information
         */
        foreach(Entity e in EntityManager.instance.entities)
        {
            ServerSend.UpdateStats(e);
            if(e.target != null)
                ServerSend.UpdateTarget(e.id, e.target.id);
        }
       
    }

    public static void PlayerMovement(int fromClient, Packet packet)
    {
        bool[] inputs = new bool[packet.ReadInt()];
        for (int i = 0; i < inputs.Length; i++)
        {
            inputs[i] = packet.ReadBool();
        }
        Quaternion rot = packet.ReadQuaternion();

        Server.clients[fromClient].player.SetInput(inputs, rot);
    }

    public static void UpdateTarget(int fromClient, Packet packet)
    {
        int targetId = packet.ReadInt();

        if(targetId == -1)
        {
            Server.clients[fromClient].player.target = null;       
        }
        else
        {
            if(EntityManager.instance.GetEntity(targetId))
                Server.clients[fromClient].player.target = EntityManager.instance.GetEntity(targetId);
        }

        ServerSend.UpdateTarget(Server.clients[fromClient].player.id, targetId);
    }

    //TODO: needs to be looked at
    public static void ToggleAttack(int fromClient, Packet packet)
    {
        bool attackToggled = packet.ReadBool();

        Server.clients[fromClient].player.GetComponent<AutoAttack>().attackToggled = attackToggled;
        ServerSend.ToggleAttack(Server.clients[fromClient].player);
    }

    public static void UseSpell(int fromClient, Packet packet)
    {
        int targetId = packet.ReadInt();
        int spellId = packet.ReadInt();

        Spells spells = Server.clients[fromClient].player.GetComponent<Spells>();
        Spell s = spells.GetSpellById(spellId);

        //no target (self cast or no target)
        if (!s.requiresTarget)
        {
            if(spells.GetSpellById(spellId) != null)
                spells.UseSpell(s);
        }
        else
        {
            if (Server.clients[fromClient].player.target != null && spells.GetSpellById(spellId) != null)
            {
                if (EntityManager.instance.GetEntity(targetId))
                    spells.UseSpell(s, EntityManager.instance.GetEntity(targetId));
            }     
        }
    }
}
