﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAttack : MonoBehaviour
{
    private Player m_owner;
    public Entity target;

    public bool attackToggled;

    private float m_attackTimer;
    private float m_attackTimerMax;

    private bool m_readyToAttack;
    public float hitRange;
    public bool canAttack;

    private void Awake()
    {
        m_owner = GetComponent<Player>();

        if (m_owner == null)
        {
            this.enabled = false;
            return;
        }

        m_attackTimerMax = m_owner.GetComponent<Stats>().attackSpeed;
        canAttack = true;
    }

    private void FixedUpdate()
    {
        //ThreadManager.ExecuteOnMainThread(() =>
        //{

        target = m_owner.target;

        if (m_attackTimer > 0)
            m_attackTimer -= Time.deltaTime;
        else
        {
            m_attackTimer = 0;
            m_readyToAttack = true;
        }

        if (target != null && attackToggled)
        {
            UpdateAutoAttack();
        }

        //});
            
    }

    private void UpdateAutoAttack()
    {
        if (m_readyToAttack && Vector3.Distance(m_owner.transform.position, target.transform.position) < hitRange)
        {
            PerformAttack(target);
            m_attackTimer = m_attackTimerMax;
            m_readyToAttack = false;
        }
    }

    private void PerformAttack(Entity target)
    {
        Stats stats = target.GetComponent<Stats>();
        if(stats != null && !stats.isDead && canAttack)
        {
            float damage = 10;
            stats.TakeDamage(damage);
            ServerSend.AutoAttack(target, GetComponent<Entity>(), damage);
        }
    }

}
