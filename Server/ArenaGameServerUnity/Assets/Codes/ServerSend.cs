﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


class ServerSend
{
    //TCP SEND

    private static void SendTCPData(int toClient, Packet packet)
    {
        packet.WriteLength();
        Server.clients[toClient].tcp.SendData(packet);
    }

    private static void SendTCPDataToAll(Packet packet)
    {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (Server.clients[i].tcp.socket != null)
                Server.clients[i].tcp.SendData(packet);
        }
    }

    private static void SendTCPDataToAll(int noClientId, Packet packet)
    {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != noClientId && Server.clients[i].tcp.socket != null)
            {
                Server.clients[i].tcp.SendData(packet);
            }
        }
    }

    //UDP SEND

    private static void SendUDPData(int toClient, Packet packet)
    {
        packet.WriteLength();
        Server.clients[toClient].udp.SendData(packet);
    }

    private static void SendUDPDataToAll(Packet packet)
    {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if(Server.clients[i].udp.endPoint != null)
                Server.clients[i].udp.SendData(packet);
        }
    }

    private static void SendUDPDataToAll(int noClientId, Packet packet)
    {
        packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != noClientId && Server.clients[i].udp.endPoint != null)
            {
                Server.clients[i].udp.SendData(packet);
            }
        }
    }

    #region packets

    public static void Welcome(int toClient, string msg)
    {
        using (Packet packet = new Packet((int)ServerPackets.welcome))
        {
            packet.Write(msg);
            packet.Write(toClient);

            SendTCPData(toClient, packet);
        }
    }

    public static void SpawnPlayer(int toClient, Player player)
    {
        using (Packet packet = new Packet((int)ServerPackets.spawnPlayer))
        {
            packet.Write(player.playerId);
            packet.Write(player.id);
            packet.Write(player.username);
            packet.Write(player.transform.position);
            packet.Write(player.transform.rotation);

            SendTCPData(toClient, packet);
        }
    }

    public static void PlayerPosition(Player player)
    {
        using (Packet packet = new Packet((int)ServerPackets.playerPosition))
        {
            packet.Write(player.playerId);
            packet.Write(player.transform.position);

            SendUDPDataToAll(packet);
        }
    }

    public static void PlayerRotation(Player player)
    {
        using (Packet packet = new Packet((int)ServerPackets.playerRotation))
        {
            packet.Write(player.playerId);
            packet.Write(player.transform.rotation);

            SendUDPDataToAll(player.playerId, packet);
        }
    }

    public static void PlayerDisconnected(int playerId)
    {
        using (Packet packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            packet.Write(playerId);

            SendTCPDataToAll(packet);
        }
    }

    public static void SpawnEntity(int entityType, int entityId, string name, Vector3 pos, Quaternion rot)
    {
        using (Packet packet = new Packet((int)ServerPackets.spawnEntity))
        {
            packet.Write(entityType);
            packet.Write(entityId);
            packet.Write(name);
            packet.Write(pos);
            packet.Write(rot);

            SendTCPDataToAll(packet);
        }
    }

    public static void SpawnEntity(int entityType, int entityId, string name, Vector3 pos, Quaternion rot, int toId)
    {
        using (Packet packet = new Packet((int)ServerPackets.spawnEntity))
        {
            packet.Write(entityType);
            packet.Write(entityId);
            packet.Write(name);
            packet.Write(pos);
            packet.Write(rot);

            SendTCPData(toId, packet);
        }
    }

    public static void EntityPosition(Entity entity)
    {
        using (Packet packet = new Packet((int)ServerPackets.entityPosition))
        {
            packet.Write(entity.id);
            packet.Write(entity.transform.position);

            SendUDPDataToAll(packet);
        }
    }

    public static void EntityRotation(Entity entity)
    {
        using (Packet packet = new Packet((int)ServerPackets.entityRotation))
        {
            packet.Write(entity.id);
            packet.Write(entity.transform.rotation);

            SendUDPDataToAll(packet);
        }
    }

    public static void EntityRemove(int entityId)
    {
        using (Packet packet = new Packet((int)ServerPackets.entityRemove))
        {
            packet.Write(entityId);

            SendTCPDataToAll(packet);
        }
    }

    public static void UpdateTarget(int entityId, int targetId)
    {
        using (Packet packet = new Packet((int)ServerPackets.updateTarget))
        {
            packet.Write(entityId);
            packet.Write(targetId);

            SendTCPDataToAll(packet);
        }
    }

    public static void UpdateStats(Entity entity)
    {
        using (Packet packet = new Packet((int)ServerPackets.updateStats))
        {
            //clients really only need to know players health and mana
            //ofc if its the player himself, he needs to know all of it

            packet.Write(entity.id);
            packet.Write(entity.stats.health);
            packet.Write(entity.stats.maxHealth);
            packet.Write(entity.stats.mana);
            packet.Write(entity.stats.maxMana);

            SendTCPDataToAll(packet);
        }
    }

    public static void ToggleAttack(Entity entity)
    {
        using (Packet packet = new Packet((int)ServerPackets.toggleAttack))
        {  
            packet.Write(entity.id);
            packet.Write(entity.GetComponent<AutoAttack>().attackToggled);

            SendTCPDataToAll(packet);
        }
    }

    public static void UseSpell(Entity entity, int spellId)
    {
        using (Packet packet = new Packet((int)ServerPackets.castSpell))
        {
            //send id, target id and spell id
            packet.Write(entity.id);
            packet.Write((entity.target == null) ? -1 : entity.target.id);
            packet.Write(spellId);

            SendTCPDataToAll(packet);
        }
    }

    public static void CastCancelled(Entity entity, bool interrupted)
    {
        using (Packet packet = new Packet((int)ServerPackets.castCancelled))
        {
            //send id, target id and spell id
            packet.Write(entity.id);
            packet.Write(interrupted);

            SendTCPDataToAll(packet);
        }
    }

    public static void CastFinished(Entity entity, int spellId, float value)
    {
        using (Packet packet = new Packet((int)ServerPackets.castFinished))
        {
            //send id, target id and spell id
            packet.Write(entity.id);
            packet.Write((entity.target == null) ? -1 : entity.target.id);
            packet.Write(spellId);
            packet.Write(value);

            SendTCPDataToAll(packet);
        }
    }

    public static void AutoAttack(Entity entity, Entity damageDealer, float value)
    {
        using (Packet packet = new Packet((int)ServerPackets.AutoAttack))
        {
            packet.Write(entity.id);
            packet.Write(damageDealer.id);
            packet.Write(value);

            SendUDPDataToAll(packet);
        }
    }
    #endregion
}
