﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Spell", menuName = "Spell", order = 1)]
public class SpellSO : ScriptableObject
{
    public int spellId;
    public string spellName;
    public string spellDesc;
    public Sprite icon;
    public float castSpeed;
    public float value;
    public float cooldownMax;
    public float manaCost;
    public float range;
    public bool requiresTarget;
    public bool castWhileMoving;
    public int applyBuffId;
    public int applyDebuffId;
}
