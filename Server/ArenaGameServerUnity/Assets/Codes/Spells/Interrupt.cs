﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interrupt : Spell
{
    //if target is casting, interrupt it!!
    public override void UseSpell(Entity user, Entity target)
    {
        if (Vector3.Distance(user.transform.position, target.transform.position) <= range)
        {
            Stats userStats = user.GetComponent<Stats>();
            Spells targetSpellInfo = target.GetComponent<Spells>();

            if (userStats.mana >= manaCost)
            {
                base.UseSpell(target);
                //TODO: trigger grafix or something

                if(targetSpellInfo.isCasting)
                {
                    targetSpellInfo.Interrupt(value);
                }

                userStats.mana -= manaCost;

                cooldown = cooldownMax;
                ServerSend.UpdateStats(user);
                //ServerSend.UpdateStats(target);
            }
        }

    }
}
