﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyBuffSpell : Spell
{
    public override void UseSpell(Entity user)
    {
        Stats userStats = user.GetComponent<Stats>();
        Buffs buffs = user.GetComponent<Buffs>();
        BuffSO buffSO = buffs.GetBuffByID(applyBuffId);

        if (buffSO != null && userStats.mana >= manaCost)
        {
            base.UseSpell(user);
            userStats.mana -= manaCost;

            Buff b = buffs.CreateBuff(buffSO);
            buffs.ApplyBuff(b, user);
          
            cooldown = cooldownMax;
            ServerSend.UpdateStats(user);
        }
       
    }

    public override void UseSpell(Entity user, Entity target)
    {
        //TODO: figure out friendly targets and buffing them etc

     /* 
        Stats userStats = user.GetComponent<Stats>();
        Buffs buffs = user.GetComponent<Buffs>();
        BuffSO buffSO = buffs.GetBuffByID(applyBuffId);

        if (buffSO != null && userStats.mana >= manaCost)
        {
            base.UseSpell(target);
            userStats.mana -= manaCost;

            Buff buffToApply = new Buff();
            buffToApply.Initialize(buffSO);
            buffs.ApplyBuff(buffToApply);

            cooldown = cooldownMax;
            ServerSend.UpdateStats(user);
        }

      */

    }
}
