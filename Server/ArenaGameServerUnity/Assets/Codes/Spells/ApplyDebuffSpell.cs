﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyDebuffSpell : Spell
{
    public override void UseSpell(Entity user, Entity target)
    {
        if (target != null && Vector3.Distance(user.transform.position, target.transform.position) <= range)
        {
            Stats userStats = user.GetComponent<Stats>();
            Stats targetStats = target.GetComponent<Stats>();

            Buffs userBuffs = user.GetComponent<Buffs>();
            Buffs targetBuffs = target.GetComponent<Buffs>();

            BuffSO buffSO = userBuffs.GetBuffByID(applyDebuffId);

            if (buffSO != null && target != null && userStats.mana >= manaCost)
            {
                base.UseSpell(target);
                userStats.mana -= manaCost;

                Buff b = userBuffs.CreateBuff(buffSO);
                targetBuffs.ApplyDebuff(b, user);
            }

            cooldown = cooldownMax;
            ServerSend.UpdateStats(user);
        } 
    }

}
