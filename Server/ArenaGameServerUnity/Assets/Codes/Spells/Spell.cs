﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Spell
{
    //TODO: Key that this is binded and other restrictions

    public int spellId;
    public string spellName;
    public string spellDesc;
    public Sprite icon;
    public float castProgress;
    public float castSpeed;
    
    public float value;

    public float cooldown;
    public float cooldownMax;

    public float manaCost;
    public float range;
    public bool requiresTarget;
    public bool castWhileMoving;
    public int applyBuffId;
    public int applyDebuffId;

    public void Initialize(SpellSO spellSO)
    {
        spellId = spellSO.spellId;
        spellName = spellSO.spellName;
        spellDesc = spellSO.spellDesc;
        icon = spellSO.icon;
        castSpeed = spellSO.castSpeed;
        value = spellSO.value;
        cooldownMax = spellSO.cooldownMax;
        manaCost = spellSO.manaCost;
        range = spellSO.range;
        requiresTarget = spellSO.requiresTarget;
        castWhileMoving = spellSO.castWhileMoving;
        applyBuffId = spellSO.applyBuffId;
        applyDebuffId = spellSO.applyDebuffId;
    }

    public virtual void UseSpell(Entity user)
    {

    }

    public virtual void UseSpell(Entity user, Entity target)
    {

    }
}
