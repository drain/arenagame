﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSpell : Spell
{
    public override void UseSpell(Entity user, Entity target)
    {
        if(target != null && Vector3.Distance(user.transform.position, target.transform.position) <= range)
        {
            Stats userStats = user.GetComponent<Stats>();
            Stats targetStats = target.GetComponent<Stats>();

            if (userStats.mana >= manaCost)
            {
                base.UseSpell(target);
                //TODO: trigger grafix or something
                targetStats.TakeDamage(value);
                userStats.mana -= manaCost;
                userStats.isDirty = true;

                if (applyDebuffId > 0)
                {
                    Buffs userBuffs = user.GetComponent<Buffs>();
                    Buffs targetBuffs = target.GetComponent<Buffs>();

                    BuffSO buffSO = user.GetComponent<Buffs>().GetBuffByID(applyDebuffId);
                    Buff b = userBuffs.CreateBuff(buffSO);
                    targetBuffs.ApplyDebuff(b, user);
                }

                cooldown = cooldownMax;
            }
        }
    }
}
