﻿using UnityEngine;
using System.Collections;

public class Heal : Spell
{
    public override void UseSpell(Entity user)
    {
        base.UseSpell(user);
        Stats stats = user.GetComponent<Stats>();

        if(stats.mana >= manaCost)
        {
            stats.Heal(value);
            stats.mana -= manaCost;

            cooldown = cooldownMax;
        }

    }
}
