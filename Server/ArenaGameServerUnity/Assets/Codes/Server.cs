﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using UnityEngine;


class Server
{
    public static int MaxPlayers { get; private set; }
    public static int Port { get; private set; }
    public static Dictionary<int, Client> clients = new Dictionary<int, Client>();
    public delegate void PacketHandler(int fromClient, Packet packet);
    public static Dictionary<int, PacketHandler> packetHandlers;

    private static TcpListener m_tcpListener;
    private static UdpClient m_udpListener;

    public const int SIO_UDP_CONNRESET = -1744830452;

    /*Start server*/
    public static void Start(int maxPlayers, int port)
    {
        MaxPlayers = maxPlayers;
        Port = port;

        Debug.Log("Starting...");
        InitializeServerData();

        m_tcpListener = new TcpListener(IPAddress.Any, port);
        m_tcpListener.Start();
        m_tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

        m_udpListener = new UdpClient(port);

        m_udpListener.Client.IOControl(
            (IOControlCode)SIO_UDP_CONNRESET,
        new byte[] { 0, 0, 0, 0 },
        null
        );

        m_udpListener.BeginReceive(UDPReceiveCallback, null);

        Debug.Log("Server started on " + port + ".");
    }

    /*Callback*/
    private static void TCPConnectCallback(IAsyncResult result)
    {
        TcpClient client = m_tcpListener.EndAcceptTcpClient(result);
        m_tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);
        Debug.Log("Incoming connection from " + client.Client.RemoteEndPoint + "...");

        for (int i = 1; i <= MaxPlayers; i++)
        {
            //slot is open
            if (clients[i].tcp.socket == null)
            {
                clients[i].tcp.Connect(client);
                clients[i].udp.Connect((IPEndPoint)client.Client.RemoteEndPoint);
                return;
            }
        }

        //server is full if we get here
        Debug.Log("Server is full!");
    }

    private static void UDPReceiveCallback(IAsyncResult result)
    {
        try
        {
            IPEndPoint clientEndPoint = new IPEndPoint(IPAddress.Any, 0);
            byte[] data = m_udpListener.EndReceive(result, ref clientEndPoint);
            m_udpListener.BeginReceive(UDPReceiveCallback, null);

            if (data.Length < 4)
                return;

            using (Packet packet = new Packet(data))
            {
                int clientId = packet.ReadInt();

                //check if we have the client with this ID and their endpoints match
                if (clientId == 0 || !clients.ContainsKey(clientId) || clients[clientId].udp.endPoint == null)
                    return;

                if (IPEndPoint.Equals(clients[clientId].udp.endPoint, clientEndPoint))
                {
                    clients[clientId].udp.HandleData(packet);
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    public static void SendUDPData(IPEndPoint clientEndPoint, Packet packet)
    {
        try
        {
            if (clientEndPoint != null)
            {
                m_udpListener.BeginSend(packet.ToArray(), packet.Length(), clientEndPoint, null, null);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private static void InitializeServerData()
    {
        for (int i = 1; i <= MaxPlayers; i++)
        {
            clients.Add(i, new Client(i));
        }

        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ClientPackets.welcomeReceived, ServerHandle.WelcomeReceived },
            { (int)ClientPackets.characterLoaded, ServerHandle.CharacterLoaded },
            { (int)ClientPackets.playerMovement, ServerHandle.PlayerMovement },
            { (int)ClientPackets.updateTarget, ServerHandle.UpdateTarget },
            { (int)ClientPackets.toggleAttack, ServerHandle.ToggleAttack },
            { (int)ClientPackets.castSpell, ServerHandle.UseSpell }
        };

        Debug.Log("Initiazed packets.");

    }

    public static void Stop()
    {
        m_tcpListener.Stop();
        //m_udpListener.Client.Shutdown(SocketShutdown.Both);
        m_udpListener.Close();
    }

}
