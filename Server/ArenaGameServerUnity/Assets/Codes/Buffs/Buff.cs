﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff
{
    public int buffId;
    public string buffName;
    public string buffDesc;
    public Sprite icon;
    public float value;
    public float tickTime;
    public float duration;

    public float tickTimer;
    public float curDuration;
    public bool isDebuff;
    public BuffType buffType;
    public BuffValue BuffValue;
    public bool canBeStacked;

    public int userId;
    public Entity owner;

    public void Initialize(BuffSO buffTemplate)
    {
        buffId = buffTemplate.buffId;
        buffName = buffTemplate.buffName;
        buffDesc = buffTemplate.buffDesc;
        icon = buffTemplate.icon;
        value = buffTemplate.value;
        tickTime = buffTemplate.tickTime;
        duration = buffTemplate.duration;
        isDebuff = buffTemplate.isDebuff;
        buffType = buffTemplate.buffType;
        BuffValue = buffTemplate.buffValue;
        canBeStacked = buffTemplate.canBeStacked;
    }

    public virtual void UpdateBuffTimer()
    {
        if(tickTimer > 0)
        {
            tickTimer -= Time.deltaTime;
        }

        if(curDuration > 0)
        {
            curDuration -= Time.deltaTime;
        }
    }

    public virtual void ApplyBuff(Entity target)
    {
        owner = target;
        curDuration = duration;
    }

    public virtual void RemoveBuff(Entity target)
    {
        owner = null;
    }

}
