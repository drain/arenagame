﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statbuff : Buff
{
    private float m_originalValue;

    public override void ApplyBuff(Entity target)
    {
        base.ApplyBuff(target);
        switch (BuffValue)
        {
            case BuffValue.Damage:
                //TODO: increase damage stat (we dont have it yet)
                break;
            case BuffValue.Health:
                m_originalValue = target.stats.maxHealth;
                float hpIncrease = m_originalValue * (value / 100);
                target.stats.maxHealth += hpIncrease;
                break;
        }
    }

    public override void RemoveBuff(Entity target)
    {
        switch (BuffValue)
        {
            case BuffValue.Damage:
                break;
            case BuffValue.Health:
                float hpIncrease = m_originalValue * (value / 100);
                target.stats.maxHealth -= hpIncrease;
                break;
        }

        base.RemoveBuff(target);
    }
}
