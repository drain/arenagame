﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Buff", menuName = "Buff", order = 2)]
public class BuffSO : ScriptableObject
{
    public int buffId;
    public string buffName;
    public string buffDesc;
    public Sprite icon;
    public float value;
    public float tickTime;
    public float duration;
    public bool isDebuff;
    public BuffType buffType;
    public BuffValue buffValue;
    public bool canBeStacked;
}

public enum BuffType
{
    None,
    HotHeal,
    Statbuff,
    StatDebuff,
    DotDamage
}

public enum BuffValue
{
    None,
    Health,
    Damage
}
