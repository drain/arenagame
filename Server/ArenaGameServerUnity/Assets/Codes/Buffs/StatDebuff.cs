﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatDebuff : Buff
{
    public override void ApplyBuff(Entity target)
    {
        base.ApplyBuff(target);
        switch(BuffValue)
        {
            case BuffValue.Damage:
                //TODO: reduce damage stat (we dont have it yet)
                break;
            case BuffValue.Health:
                
                break;
        }
    }

    public override void RemoveBuff(Entity target)
    {
        switch (BuffValue)
        {
            case BuffValue.Damage:
                break;
            case BuffValue.Health:
                break;
        }

        base.RemoveBuff(target);
    }
}
