﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotHeal : Buff
{
    public override void UpdateBuffTimer()
    {
        base.UpdateBuffTimer();

        if(tickTimer <= 0)
        {
            Stats stats = owner.GetComponent<Stats>();
            stats.Heal(value);
            tickTimer = tickTime;
        }
    }
}
