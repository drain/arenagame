﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotDamage : Buff
{
    public override void UpdateBuffTimer()
    {
        base.UpdateBuffTimer();

        if (tickTimer <= 0)
        {
            Stats stats = owner.GetComponent<Stats>();
            stats.TakeDamage(value);
            tickTimer = tickTime;
        }
    }
}
