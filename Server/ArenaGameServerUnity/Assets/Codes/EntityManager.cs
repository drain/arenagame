﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour
{
    public static EntityManager instance;
    public static int last_id = 0;

    //list that hold all entities in the game world (players, npcs, player pets, etc.)
    public List<Entity> entities;

    public List<NPCPrefab> npcsToSpawn = new List<NPCPrefab>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }

        entities = new List<Entity>();
    }

    private void Start()
    {
        SpawnNPCs();
    }

    //return entity based on its id (this is not playerId!!)
    public Entity GetEntity(int id)
    {
        for(int i = 0; i < entities.Count; i++)
        {
            if (entities[i].id == id)
                return entities[i];
        }

        return null;
    }

    public void AddEntity(Entity e)
    {
        last_id++;
        e.id = last_id;
        entities.Add(e);
    }

    public bool ContainsEntity(int id)
    {
        for (int i = 0; i < entities.Count; i++)
            if (id == entities[i].id)
                return true;

        return false;
    }

    //used for anything that is not player (npcs, player pets, etc.)
    public Entity SpawnEntity(Entity e, Vector3 pos)
    {
        return Instantiate(e.gameObject, pos, Quaternion.identity).GetComponent<Entity>();
    }

    public void RemoveEntity(int id)
    {
        Entity e = GetEntity(id);
        ServerSend.EntityRemove(id);
        entities.Remove(e);
    }

    public void SpawnNPCs()
    {
        for(int i = 0; i < npcsToSpawn.Count; i++)
        {
            Entity e = SpawnEntity(npcsToSpawn[i].npc, npcsToSpawn[i].spawnPos);
            NPC npc = (NPC)e;
            npc.transform.Rotate(new Vector3(0, 180, 0));
            ServerSend.SpawnEntity(npc.prefabId, npc.id, e.eName, e.transform.position, e.transform.rotation);
        }
    }
}

[System.Serializable]
public class NPCPrefab
{
    public NPC npc;
    public Vector3 spawnPos;
}
