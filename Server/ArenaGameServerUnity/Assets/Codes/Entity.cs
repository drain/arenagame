﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Stats), typeof(Spells), typeof(Buffs))]
public class Entity : MonoBehaviour
{
    public int id;
    public string eName;
    public Entity target;
    public Stats stats;
    public Spells spells;
    public AutoAttack autoAttack;
    //public bool playerControlled;

    protected virtual void Awake()
    {
        stats = GetComponent<Stats>();
        spells = GetComponent<Spells>();
        autoAttack = GetComponent<AutoAttack>();
    }

    protected virtual void Start()
    {
        if(!EntityManager.instance.ContainsEntity(id))
        {
            EntityManager.instance.AddEntity(this);
        }
    }
}
